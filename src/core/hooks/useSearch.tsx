import axios, { CancelToken } from "axios";
import { useEffect, useState } from "react";


export function useSearch<TParams, T>(
  requesterFn: (query: TParams, token?: CancelToken) => Promise<T>,
  initialQuery: TParams
) {
  const [results, setResults] = useState<T | undefined>(undefined);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");
  const [params, setParams] = useState<TParams>(initialQuery);

  useEffect(() => {
    if (!params) {
      return;
    }
    const token = axios.CancelToken.source();
    (async () => {
      try {
        setMessage("");
        setLoading(true);
        setParams(params);
        const results = await requesterFn(params, token.token);
        setResults(results);
      } catch (error) {
        if (error instanceof axios.Cancel) {
          return;
        }
        setMessage(error.message);
      } finally {
        setLoading(false);
      }
    })();

    /* Can't perform a React state update on an unmounted component. 
    This is a no-op, but it indicates a memory leak in your application.
    To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function. */
    return () => token.cancel("Cancelled");
  }, [params]);

  // return [{x:1},()=>{}] as [object,Function]
  return [
    {
      query: params,
      loading,
      message,
      results,
    },
    (query: TParams) => {
      setParams(query);
    },
  ] as const;
}
