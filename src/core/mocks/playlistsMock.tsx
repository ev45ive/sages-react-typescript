import { Playlist } from "../model/Playlist";

export const playlistsMock: Playlist[] = [
  {
    id: "123",
    type: "playlist",
    name: "Playlist 123",
    public: true,
    description: "OPis...",
  },
  {
    id: "234",
    type: "playlist",
    name: "Playlist 234",
    public: false,
    description: "OPis...",
  },
  {
    id: "345",
    type: "playlist",
    name: "Playlist 345",
    public: true,
    description: "OPis...",
  },
];
