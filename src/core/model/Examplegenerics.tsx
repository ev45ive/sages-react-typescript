export {};

const x1: string[] = ["a", "b"];
const x2: Array<string> = ["a", "b"];
// const x3: Array<number> = ['a', 'b'] // Type 'string' is not assignable to type 'number'.
const x4: Array<string | number> = ["a", 123, "c"];
const x7: (string | number)[] = ["a", 123, "c"];

const x5 = ["a", "b"];
const x6 = ["a", 123];
const x8: Array<Array<string>> = [
  ["X", "O", " "],
  ["O", "X", " "],
  [" ", "X", "O"],
];

type Ref<T> = {
  current: T | null;
  // previous?: T | null
};
const ref1: Ref<number> = { current: 123 };
const ref2: Ref<string> = { current: "123" };

function getValue<T>(ref: Ref<T>): T | null {
  return ref.current;
}

// Useless Generics - no relationship:
declare function parse<T>(data: string): T;
// parse('...') as Playlist
// const p:Playlist = parse(...)
declare function serialize<T>(entity: T): string;

// Usefull Generics - checks relationship:
function identity<T>(id: T): T {
  return id;
}

function getFirst<T>(arr: T[]): T {
  return arr[0];
}
const result = ["123"];
// const result = [123]

const f1 = getFirst<string>(result);
const f2 = getFirst(result);
const f3: number = getFirst([123]);
const f4 = getFirst([]);

// const arr = [] // .push(123).push('abc')...
// const f5 = getFirst(arr) // any

// Doesnt work with JSX:

// const getFirst2 = <T>(arr: T[]): T => {
//     return arr[0]
// }

// const getFirst3: <T>(arr: T[]) => T = (arr) => {
//     return arr[0]
// }

type GetFirstType = <T>(arr: T[]) => T;

const getFirst4: GetFirstType = (arr) => {
  return arr[0];
};

class Queue<T> {
  data: T[] = [];
  push(item: T) {
    this.data.push(item);
  }
  pop() {
    return this.data.shift();
  }
}

type PaginatedObject<K, T extends { type: K }> = {
  kind: K;
  items: T[];
  total: number;
};
const paginated: PaginatedObject<"album", { type: "album" }> = {
  kind: "album",
  items: [{ type: "album" }],
  total: 1,
};

function getId<T extends { id: string }>(obj: T): T["id"] {
  return obj.id;
}
getId({ id: "123", name: "Album" });

// return tuple
function pair<T, U2>(p: T, p2: U2): [T, U2] {
  return [p, p2];
  // or:   return [p, p2] as const
}

type mode = "details" | "edit";
// console.log(mode); // 'mode' only refers to a type, but is being used as a value here.

enum Role {
  admin = "ADMIN_ROLE",
  user = "USER_ROLE",
  moderator = "MODERATOR_ROLE",
}
const role: Role = Role.admin;
console.log(Role);

const selectedRole = "admin";
Role[selectedRole] == "ADMIN_ROLE";

type roles = keyof typeof Role;

// function getRole<TName extends keyof typeof Role>(roles: typeof T, role: TName) {
function getRole<T extends object, TName extends keyof T>(
  roles: T,
  role: TName
) {
  return roles[role];
}
// getRole({ x: "1" },'x') == '1'; // object is mutable, x can change
// getRole(Role,'user') == 'ADMIN_ROLE' // This condition will always return 'false' since the types 'Role.user' and '"ADMIN_ROLE"' have no overlap.
getRole(Role, "user") == "USER_ROLE";
getRole({...Role, extra:'EXTRA_ROLE'}, 'extra') == "USER_ROLE";

interface User{ id:string, role:Role}
interface Admin{ id:string, role:Role.admin, isAdmin:true}
interface Moderator{ id:string, role:Role.moderator, isModerator:true}

function getUserRole<T extends User>(user:T):T['role'] {
    return user.role
}
const u1 = getUserRole({} as Admin)
// u1 == Role.user // This condition will always return 'false' since the types 'Role.admin' and 'Role.user' have no overlap.
