import { Playlist } from "./Playlist"
import { Album, Track } from "./Search"

type MaybeString = string | number

function render(result: MaybeString) {
    // TypeGuard
    if (typeof result === 'string') {
        result.toLocaleUpperCase()
    } else {
        result.toFixed(2)
    }
}

const playlist: Playlist = {} as Playlist

if (playlist.tracks) playlist.tracks.length
const len1 = playlist.tracks ? playlist.tracks.length : 0
const len2 = playlist.tracks?.length
const len3 = playlist.tracks?.length || 0
const len4 = playlist.tracks && playlist.tracks.length
const len5 = playlist.tracks?.length ?? 0

// Nominal vs Structural Typing

interface Point { x: number; y: number }
interface Vector { x: number; y: number, length: number }
// interface Vector { x: number; y: number, length?: number }

let p: Point = { x: 123, y: 123 }
let v: Vector = { x: 123, y: 123, length: 123 }

p = v // OK 
// v = p // Property 'length' is missing in type 'Point' but required in type 'Vector'.

// p.length // Property 'length' does not exist on type 'Point' in TS, but it does in JS

// Indexed types

const obj = {
    [1 + 2 + 3]: 123
}

interface MYDictionary {
    [haslo: string]: string
}
const dict: MYDictionary = {}
dict.a = '1'
// dict.b = 123 // Type 'number' is not assignable to type 'string'

interface PlaylistCache {
    [playlist_id: string]: Playlist
}

type SearchResult = Track | Playlist  // | Album
// type Groups = 'track' | 'playlist'
type Groups = SearchResult['type']

type ResultsGroup = {
    [group in Groups]: string[]
}

const groups: ResultsGroup = {
    playlist: ['123'],
    track: [],
}