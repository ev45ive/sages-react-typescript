import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import {
  // HashRouter as Router,
  BrowserRouter as Router,
} from "react-router-dom";
import { UserContextProvider } from "./core/contexts/UserContext";
import { Provider } from "react-redux";
import { store } from "./core/store";

ReactDOM.render(
  <React.StrictMode>
    {/* <TranslationProvider> */}
    {/* <ThemeContextProvider> */}
    <UserContextProvider>
      <Provider store={store}>
        <Router>
          <App />
        </Router>
      </Provider>
    </UserContextProvider>
    {/* </ThemeContextProvider> */}
    {/* </TranslationProvider> */}
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
