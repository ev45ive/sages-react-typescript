import React from "react";
import { SimpleAlbum } from "../../core/model/Search";
import { useHistory } from "react-router-dom";
interface Props {
  album: SimpleAlbum;
}

export const AlbumCard: React.FC<Props> = ({ album, children }) => {
  const { push } = useHistory();

  return (
    <div className="card">
      <img src={album.images[0].url} className="card-img-top" alt="..." />

      <div className="card-body">
        <h5
          className="card-title"
          onClick={() => {
            push(`/search/albums/${album.id}`);
          }}
        >
          {album.name}
        </h5>
        {children}
        {/* <p className="card-text">
                This is a longer card with supporting text below as a natural
                lead-in to additional content. This content is a little bit
                longer.
              </p> */}
      </div>
    </div>
  );
};
