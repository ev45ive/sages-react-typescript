import {
  fireEvent,
  getByRole,
  render,
  screen,
} from "@testing-library/react";
import { Playlist } from "../../core/model/Playlist";
import { PlaylistList } from "./PlaylistList";

// Zero One Inifnity Loading Error
export {};

describe("PlaylistList", () => {
  const playlistsMock: Playlist[] = [
    {
      id: "123",
      type: "playlist",
      name: "Playlist 123",
      public: true,
      description: "OPis...",
    },
    {
      id: "234",
      type: "playlist",
      name: "Playlist 234",
      public: false,
      description: "OPis...",
    },
    {
      id: "345",
      type: "playlist",
      name: "Playlist 345",
      public: true,
      description: "OPis...",
    },
  ];

  const setup = ({ playlists = playlistsMock, selected = "" }) => {
    const onSelectSpy = jest.fn();
    const onRemoveSpy = jest.fn();
    render(
      <PlaylistList
        playlists={playlists}
        selected={selected}
        onSelect={onSelectSpy}
        onDelete={onRemoveSpy}
      />
    );
    return { onSelectSpy, onRemoveSpy };
  };

  test("shows no playlists", () => {
    setup({ playlists: [] });

    const tab = screen.queryByRole("tab", {});
    expect(tab).not.toBeInTheDocument();
  });

  test("shows playlists", () => {
    setup({});

    const tabs = screen.queryAllByRole("tab", {});
    expect(tabs).toHaveLength(3);

    screen.getByRole("tab", { name: /Playlist 123/ });
    // expect(tabs[0]).toHaveTextContent('Playlist 123')

    // logRoles(document.body);
  });

  test("shows currently selected playlist", () => {
    setup({
      selected: "234",
    });
    const tabs = screen.getAllByRole("tab", {
      selected: true,
    });
    expect(tabs).toHaveLength(1);
    expect(tabs[0]).toHaveClass("active");
    expect(tabs[0]).toHaveTextContent(/Playlist 234/);
  });

  test("playlist button emits selected id", () => {
    const { onSelectSpy } = setup({});
    const btn = screen.getByRole("tab", { name: /Playlist 123/ });
    fireEvent.click(btn);
    expect(onSelectSpy).toHaveBeenCalledWith("123");
  });

  test("remove button emits remove event with playlist id", () => {
    const { onRemoveSpy } = setup({});
    const tab = screen.getByRole("tab", { name: /Playlist 123/ });

    const btn = getByRole(tab, "button", { name: "Remove Playlist" });

    fireEvent.click(btn);
    expect(onRemoveSpy).toHaveBeenCalledWith("123");
  });
});
